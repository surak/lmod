#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/pkg-info.mk

%: PATH	:= $(PATH):$(CURDIR)/hermes/bin
%:
	dh $@

override_dh_auto_configure-arch:
	dh_auto_configure -- \
		--prefix=/usr/share \
		--with-ModulePathInit=/etc/lmod/modulespath \
		PS=/bin/ps \
		READLINK=/bin/readlink \
		PATH_TO_LS=/bin/ls \
		PATH_TO_PAGER=/bin/more \

override_dh_auto_configure-indep:

execute_after_dh_auto_build-indep:
	$(MAKE) -C docs

override_dh_auto_test:
	id -u
	dh_auto_test || echo $$? > debian/tests-failed.stamp
	@if test -f debian/tests-failed.stamp ; then \
		for dir in rt/*/ ; do \
			echo ">>>> $${dir}err.txt" ; \
			diff -u $${dir}t1/*/_err.left $${dir}t1/*/_err.right || echo ; \
			echo ">>>> $${dir}out.txt" ; \
			diff -u $${dir}out.txt $${dir}t1/*/out.txt || echo ; \
		done ; \
	fi
	test ! -f debian/tests-failed.stamp || exit $$(cat debian/tests-failed.stamp)

execute_after_dh_auto_install-arch:
	mkdir -p debian/tmp/etc/profile.d
	cp debian/tmp/usr/share/lmod/lmod/init/profile debian/tmp/etc/profile.d/lmod.sh
	# move the shared library from /usr/share to /usr/lib
	mkdir -p debian/tmp/usr/lib/lmod/$(DEB_VERSION_UPSTREAM)
	mv debian/tmp/usr/share/lmod/$(DEB_VERSION_UPSTREAM)/lib debian/tmp/usr/lib/lmod/$(DEB_VERSION_UPSTREAM)/
	ln -s /usr/lib/lmod/$(DEB_VERSION_UPSTREAM)/lib debian/tmp/usr/share/lmod/$(DEB_VERSION_UPSTREAM)/lib

execute_after_dh_auto_clean:
	$(RM) -r docs/build
	test ! -d testreports || testcleanup

override_dh_clean:
	dh_clean -XTAGS -XTACC/TACC.orig -XTACC/cluster.orig
